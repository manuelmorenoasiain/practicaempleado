package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import clases.Departamento;
import clases.Empleado;
import consultas.Consultas;

@WebServlet("/Inicio")
public class PracticaEmpleado extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    public PracticaEmpleado() {
        super();
    }
    
    Consultas consultas = new Consultas();
    ArrayList<Empleado> empleados = new ArrayList<Empleado>();
    ArrayList<Departamento> alDept;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			alDept = consultas.getDepartamentos();
			empleados = consultas.mostrarEmpleados();
			request.setAttribute("mostrarEmpleados", empleados);
			request.setAttribute("alDept", alDept);
		} catch (ClassNotFoundException | SQLException e) {
			String error_empleado = "<div class='alert alert-danger' role='alert'>No se ha podido conectar con la base de datos.</div>";
			request.setAttribute("error_empleado", error_empleado);
		}
		String pagina = "/inicio.jsp";
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(pagina);
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {        
		response.setContentType("text/html;charset=UTF-8");
		
		String valorEliminarEmp = request.getParameter("eliminarEmpleado");
        String mostrarTodos = request.getParameter("mostrarTodos");
        String buscarEmpNom = request.getParameter("buscarEmpNom");
        String valorBuscarEmpSalMin = request.getParameter("buscarEmpSalMin");
        String valorBuscarEmpSalMax = request.getParameter("buscarEmpSalMax");
        String buscarEmpFechaMin = request.getParameter("buscarEmpFechaMin");
        String buscarEmpFechaMax = request.getParameter("buscarEmpFechaMax");
        String depto = request.getParameter("depto");
        String buscar = request.getParameter("buscar");
        
        int buscarEmpSalMax = 0;
        int buscarEmpSalMin = 0;
        
        String pagina = "/inicio.jsp";
        
	    if(mostrarTodos != null) { 	
            	doGet(request, response);
            } else if(valorEliminarEmp != null) {
    			int eliminarEmpleado = Integer.parseInt(valorEliminarEmp);
    			try {
    				consultas.eliminarEmpleado(eliminarEmpleado);
    				request.setAttribute("eliminarCorrecto", "<div class='alert alert-success' role='alert'>Empleado "+eliminarEmpleado+" eliminado con éxito.</div>");
    				doGet(request, response);
    			} catch(MySQLIntegrityConstraintViolationException e) {
    				request.setAttribute("eliminarFallido", "<div class='alert alert-danger' role='alert'>No se puede eliminar a un empleado que tiene empleados a su cargo.</div>");
    				doGet(request, response);
    				e.printStackTrace();
    			} catch (ClassNotFoundException | SQLException e) {
    				request.setAttribute("eliminarFallido", "<div class='alert alert-danger' role='alert'>Error.</div>");
    				doGet(request, response);
    				e.printStackTrace();
    			}
    		} else if(buscar != null) {
    			if(buscarEmpNom.isEmpty()) {
    				buscarEmpNom = "";
    			}
        		if(valorBuscarEmpSalMin.isEmpty()) {
        			valorBuscarEmpSalMin = "0";
        		}
        		if(valorBuscarEmpSalMax.isEmpty()) {
        			valorBuscarEmpSalMax = "9999";
        		}
        		
        		
        		buscarEmpSalMin = Integer.parseInt(valorBuscarEmpSalMin);
        		buscarEmpSalMax = Integer.parseInt(valorBuscarEmpSalMax);
        		
        		try {
        			request.setAttribute("mostrarEmpleados", consultas.buscarEmpleado(buscarEmpNom.toUpperCase(), buscarEmpSalMin, buscarEmpSalMax, depto));
        			request.setAttribute("alDept", alDept);
        			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(pagina);
        			dispatcher.forward(request, response);
    			} catch (ClassNotFoundException | SQLException e) {
    				e.printStackTrace();
    			}
        	} else {
        		doGet(request, response);
        	}
	}
}
