package servlets;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import clases.Departamento;
import clases.Empleado;
import clases.Validaciones;
import consultas.Consultas;

@WebServlet("/ModificarEmpleado")
public class ModificarEmpleado extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ModificarEmpleado() {
        super();
    }
    
    Consultas consultas = new Consultas();
    Empleado emp;
    ArrayList<Departamento> alDept;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String modEmpleado = request.getParameter("modEmpleado");
		try {
			int numEmpleado = Integer.parseInt(modEmpleado);
			for(int i=0; i<consultas.mostrarEmpleados().size(); i++) {
        		if(consultas.mostrarEmpleados().get(i).getId() == numEmpleado) {
        			emp = consultas.mostrarEmpleados().get(i);
        			request.setAttribute("empleado", emp);
        		}
        	}
			alDept = consultas.getDepartamentos();
			request.setAttribute("alDept", alDept);
		} catch (ClassNotFoundException | SQLException e) {
			String error_departamentos = "ERROR, no se han podido mostrar los departamentos";
			request.setAttribute("error_departamentos", error_departamentos);
		}
		String pagina = "/modificarEmpleado.jsp";
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(pagina);
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Validaciones validacion = new Validaciones();
		
		String valorId = request.getParameter("id");
        String nombre = request.getParameter("nombre");
        String oficio = request.getParameter("oficio");
        String valorManager = request.getParameter("manager");
        String valorFechaContratacion = request.getParameter("fechaContratacion");
        String valorSalario = request.getParameter("salario");
        String valorComision = request.getParameter("comision");
        String depto = request.getParameter("depto");
        
        if(!valorId.isEmpty() && !valorManager.isEmpty() && !depto.isEmpty()) {
        	int id = Integer.parseInt(valorId);
        	int manager = 0;
        	Date fecha = null;
        	float salario = 0;
        	float comision = 0;
        	
        	try {
				if(!valorManager.isEmpty()) {
					manager = validacion.chequearManager(id, valorManager, request);
				}
				if(!valorSalario.isEmpty()) {
					salario = validacion.chequearSalario(valorSalario, request);
				}
				if(!valorComision.isEmpty()) {
					comision = validacion.chequearComision(valorComision, request);
				}
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
        	validacion.chequearNombre(nombre, request);
        	validacion.chequearOficio(oficio, request);
        	
        	if(!valorFechaContratacion.isEmpty()) {
        		fecha = validacion.chequearFecha(valorFechaContratacion, request);
        	} else {
        		valorFechaContratacion = "0000-00-00";
        		fecha = validacion.chequearFecha(valorFechaContratacion, request);
        	}
        	
        	if(validacion.getErrores() == 0) {
        		Empleado emp = new Empleado(id, nombre, oficio, manager, fecha, salario, comision, depto);
        		try {
					consultas.modificarEmpleado(emp);
					request.setAttribute("formCorrecto", "<div class='alert alert-success' role='alert'>Empleado modificado correctamente.</div>");
				} catch (ClassNotFoundException | SQLException e) {
					request.setAttribute("formIncorrecto", "<div class='alert alert-danger' role='alert'>Error.</div>");
					e.printStackTrace();
				}
        	}
        	doGet(request, response);
        } else {
        	request.setAttribute("errorBD", "<div class='alert alert-danger' role='alert'>Debes rellenar todos los campos obligatorios *.</div>");
        	doGet(request, response);
        }
	        
	}

}
