package consultas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import clases.Departamento;
import clases.Empleado;

public class Consultas {
	
	public static Connection conexion() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conexion = null;
		conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/scott", "root", "");
		return conexion;
	}
	
	public ArrayList<Empleado> mostrarEmpleados() throws ClassNotFoundException, SQLException {
		Statement st = conexion().createStatement();
		ResultSet rs = st.executeQuery("SELECT emp.empno, emp.ename, emp.job, emp.mgr, "
				+ "emp.hiredate, emp.sal, emp.comm, dept.dname FROM emp LEFT JOIN dept ON emp.deptno = dept.deptno");
		
		ArrayList<Empleado> alEmpleados = new ArrayList<Empleado>();
		while(rs.next()) {
			alEmpleados.add(new Empleado(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), 
					rs.getDate(5), rs.getFloat(6), rs.getFloat(7), rs.getString(8)));
		}
		conexion().close();
		return alEmpleados;
	}
	
	public void eliminarEmpleado(int id) throws ClassNotFoundException, SQLException {
		Statement st = conexion().createStatement();
		st.executeUpdate("DELETE FROM emp WHERE empno = "+id+"");
	}
	
	public ArrayList<Empleado> buscarEmpleado(String nombre, int buscarEmpSalMin, int buscarEmpSalMax, String depto) throws ClassNotFoundException, SQLException {
		Statement st = conexion().createStatement();
		ResultSet rs;
		
		if(nombre == "") {
			nombre = "ANY(SELECT ename FROM emp)";
		} else {
			nombre = "'"+nombre+"'";
		}
		
		if(depto == "") {
			depto = "ANY(SELECT dname FROM dept)";
		} else {
			depto = "'"+depto+"'";
		}
		
		rs = st.executeQuery("SELECT emp.empno, emp.ename, emp.job, emp.mgr, emp.hiredate, emp.sal, emp.comm, dept.dname "
				+ "FROM emp LEFT JOIN dept ON emp.deptno = dept.deptno "
				+ "WHERE emp.ename="+nombre+" AND sal BETWEEN "+buscarEmpSalMin+" "
								+ "AND "+buscarEmpSalMax+" AND dept.dname="+depto+"");
		
		ArrayList<Empleado> alEmpleados = new ArrayList<Empleado>();
		
		while(rs.next()) {
			alEmpleados.add(new Empleado(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), 
					rs.getDate(5), rs.getFloat(6), rs.getFloat(7), rs.getString(8)));
		}
		conexion().close();
		return alEmpleados;
	}
	
	public void agregarEmpleado(Empleado emp) throws ClassNotFoundException, SQLException {
		Statement st = conexion().createStatement();
		int dept = 0;
		
		if(emp.getDepartment() != "") {
			ResultSet deptno = st.executeQuery("SELECT deptno FROM dept WHERE dname='"+emp.getDepartment()+"'");
			
			while(deptno.next()) {
				dept = deptno.getInt(1);
			}
		}

		st.executeUpdate("INSERT INTO emp VALUES('"+emp.getId()+"', '"+emp.getName()+"', '"+emp.getJob()+"', '"+emp.getManager()+"', '"+emp.getHiredate()+"', '"+emp.getSalary()+"', '"+emp.getCommission()+"', '"+dept+"')");
		
		conexion().close();
	}

	public ArrayList<Departamento> getDepartamentos() throws ClassNotFoundException, SQLException {
		Statement st = conexion().createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM dept");
		
		ArrayList<Departamento> alDept = new ArrayList<Departamento>();
		
		while(rs.next()) {
			alDept.add(new Departamento(rs.getInt(1), rs.getString(2)));
		}
		conexion().close();
		return alDept;
	}
	
	public void modificarEmpleado(Empleado emp) throws ClassNotFoundException, SQLException {
		Statement st = conexion().createStatement();
		ResultSet deptno = st.executeQuery("SELECT deptno FROM dept WHERE dname='"+emp.getDepartment()+"'");
		int dept = 0;
		while(deptno.next()) {
			dept = deptno.getInt(1);
		}
		
		st.executeUpdate("UPDATE emp "
				+ "SET ename='"+emp.getName()+"', job='"+emp.getJob()+"', mgr="+emp.getManager()+", "
						+ "hiredate='"+emp.getHiredate()+"', sal="+emp.getSalary()+", comm="+emp.getCommission()+", deptno="+dept+""
								+ " WHERE empno="+emp.getId()+";");
		
		
		conexion().close();
	}
}
