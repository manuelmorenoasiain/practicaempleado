package clases;

import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import consultas.Consultas;

public class Validaciones {
	int errores = 0;
	Consultas consulta = new Consultas();
	
	public int chequearId(String valorId, HttpServletRequest request) throws ClassNotFoundException, SQLException {
		int id = 0;
		try {
    		id = Integer.parseInt(valorId);
    		
    		if(id < 1 || id > 9999) {
    			request.setAttribute("errorId", "<div class='alert alert-danger' role='alert'>El ID solo puede ser de 1 a 9999.</div>");
				errores++;
    		} else {
    			for(int i=0; i<consulta.mostrarEmpleados().size(); i++) {
        			if(consulta.mostrarEmpleados().get(i).getId() == id) {
        				request.setAttribute("errorId", "<div class='alert alert-danger' role='alert'>El ID introducido ya pertenece a un empleado.</div>");
        				errores++;
        			}
        		}
    		}
    	} catch(NumberFormatException e) {
    		request.setAttribute("errorId", "<div class='alert alert-danger' role='alert'>El ID no es v�lido.</div>");
    		errores++;
    	}
		return id;		
	}
	
	public float chequearSalario(String valorSalario, HttpServletRequest request) throws ClassNotFoundException, SQLException {
		float salario = 0;
		try {
			salario = Float.parseFloat(valorSalario);
    	} catch(NumberFormatException e) {
    		request.setAttribute("errorSalario", "<div class='alert alert-danger' role='alert'>El salario debe ser un n�mero.</div>");
    		errores++;
    	}
		return salario;
	}

	public void chequearNombre(String nombre, HttpServletRequest request) {
		if(nombre.length() > 30) {
			request.setAttribute("errorNombre", "<div class='alert alert-danger' role='alert'>El nombre solo puede tener 30 caracteres como m�ximo.</div>");
			errores++;
		}
	}
	
	public void chequearOficio(String oficio, HttpServletRequest request) {
		if(oficio.length() > 30) {
			request.setAttribute("errorOficio", "<div class='alert alert-danger' role='alert'>El oficio solo puede tener 30 caracteres como m�ximo.</div>");
			errores++;
		}
	}
	
	public Date chequearFecha(String valorFecha, HttpServletRequest request) {
		Date fecha = null;
		try {
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date = sdf1.parse(valorFecha);
			fecha = new Date(date.getTime());
		} catch (ParseException e) {
			request.setAttribute("errorFecha", "<div class='alert alert-danger' role='alert'>La fecha no es v�lida.</div>");
			errores++;
		}
		return fecha;
	}

	public float chequearComision(String valorComision, HttpServletRequest request) throws ClassNotFoundException, SQLException {
		float comision = 0;
		try {
			comision = Float.parseFloat(valorComision);
		} catch(NumberFormatException e) {
    		request.setAttribute("errorComision", "<div class='alert alert-danger' role='alert'>La comisi�n debe ser un n�mero.</div>");
    		errores++;
		}
		return comision;
	}

	public int chequearManager(int id, String valorManager, HttpServletRequest request) throws ClassNotFoundException, SQLException {
		int manager = 0;
		boolean managerInexistente = true;
		try {
			manager = Integer.parseInt(valorManager);
			if(manager < 0 || manager > 9999) {
				request.setAttribute("errorManager", "<div class='alert alert-danger' role='alert'>El m�nager debe ser un n�mero entre 0 y 9999.</div>");
	    		errores++;
			}
			if(manager == id) {
				request.setAttribute("errorManager", "<div class='alert alert-danger' role='alert'>El m�nager y el ID no pueden ser iguales.</div>");
	    		errores++;
			}
			for(int i=0; i<consulta.mostrarEmpleados().size(); i++) {
    			if(consulta.mostrarEmpleados().get(i).getId() == manager || manager == 0) {
    				managerInexistente = false;
    			}
    		}
			if(managerInexistente == true) {
				manager = 0;
				request.setAttribute("errorId", "<div class='alert alert-danger' role='alert'>El m�nager no existe.</div>");			
				errores++;
			}
    	} catch(NumberFormatException e) {
    		request.setAttribute("errorManager", "<div class='alert alert-danger' role='alert'>El m�nager debe ser un n�mero.</div>");
    		errores++;
    	}
		return manager;
	}

	public int getErrores() {
		return errores;
	}
}
