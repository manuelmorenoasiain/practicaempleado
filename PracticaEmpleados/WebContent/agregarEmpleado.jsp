<%@ page language="java" contentType="text/html; charset=UTF-8"
     pageEncoding="UTF-8"%>
    <%@ page import ="java.util.ArrayList"%>
    <%@ page import ="clases.Empleado"%>
    <%@ page import ="clases.Departamento"%>
    <%@ page import ="java.util.LinkedHashMap"%>
    <%@ page import ="java.util.Iterator"%>
	<%
	String error_departamentos = (String) request.getAttribute("error_departamentos");
	if(error_departamentos == null) {
		error_departamentos = "";
	}
	
	ArrayList<Departamento> alDept = new ArrayList<Departamento>();  
	alDept = (ArrayList<Departamento>) request.getAttribute("alDept");

	String errorForm = (String) request.getAttribute("errorBD");
	if(errorForm == null) {
		errorForm = "";
	}
	String formCorrecto = (String) request.getAttribute("formCorrecto");
	if(formCorrecto == null) {
		formCorrecto = "";
	}
	String formIncorrecto = (String) request.getAttribute("formIncorrecto");
	if(formIncorrecto == null) {
		formIncorrecto = "";
	}
	String errorFecha = (String) request.getAttribute("errorFecha");
	if(errorFecha == null) {
		errorFecha = "";
	}
	String errorId = (String) request.getAttribute("errorId");
	if(errorId == null) {
		errorId = "";
	}
	String errorSalario = (String) request.getAttribute("errorSalario");
	if(errorSalario == null) {
		errorSalario = "";
	}
	String errorComision = (String) request.getAttribute("errorComision");
	if(errorComision == null) {
		errorComision = "";
	}
	String errorNombre = (String) request.getAttribute("errorNombre");
	if(errorNombre == null) {
		errorNombre = "";
	}
	String errorOficio = (String) request.getAttribute("errorOficio");
	if(errorOficio == null) {
		errorOficio = "";
	}
	String errorManager = (String) request.getAttribute("errorManager");
	if(errorManager == null) {
		errorManager = "";
	}
	%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Añadir empleado</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script><link rel="stylesheet" href="/PracticaEmpleados/agregarEmpleado.css" type="text/css">
</head>
<body>
	<div id="header">
		<h1>AÑADIR EMPLEADO</h1>
	</div>
    <div class="container">
    	<%=formCorrecto %>
    	<%=formIncorrecto %>
    	<%=errorId %>
    	<%=errorNombre %>
    	<%=errorOficio %>
    	<%=errorManager %>
    	<%=errorFecha %>
    	<%=errorSalario %>
    	<%=errorComision %>
    	<%=errorForm %>
    	<form action="Inicio">
	    	<button type="submit" class="btn btn-success">Volver a inicio</button>
	    </form>
    	<form method="POST" action="AgregarEmpleado" id="FormularioAgregar">
	    	<div class="row">
	    		<div class="col-sm" class="form-group">
	    			<label for="id">ID*</label>
	                <input class="form-control" type="text" name="id" />
	    		</div>
	    		<div class="col-sm">
	    			<label for="nombre">Nombre</label>
	                <input class="form-control" type="text" name="nombre" />
	    		</div>
	    		<div class="col-sm">
	    			<label for="oficio">Oficio</label>
	                <input class="form-control" type="text" name="oficio" />
	    		</div>  		
	    	</div>
	    	<div class="row">
	    		<div class="col-sm">
	    			<label for="manager">Mánager*</label>
	                <input class="form-control" type="text" name="manager" />
	    		</div>
	    		<div class="col-sm">
	    			<label for="fechaContratacion">Fecha de contratación</label>
	                <input class="form-control" type="text" name="fechaContratacion" />
	    		</div>
	    		<div class="col-sm">
	    			<label for="salario">Salario</label>
	                <input class="form-control" type="text" name="salario" />
	    		</div>
	    	</div>
	    	<div class="row">
	    		<div class="col-sm">
	    			<label for="comision">Comisión</label>
	                <input class="form-control" type="text" name="comision" />
	    		</div>
	    		<%=error_departamentos %>
	    		<div class="col-sm">
	   				<label for="depto">Departamento*</label>
	   				<select class="form-control" name="depto">
	               		<option value="">Selecciona un departamento...</option>
	               		<%for(int i=0; i<alDept.size(); i++) {
	               			%><option value="<%=alDept.get(i).getName()%>"><%=alDept.get(i).getName()%></option><%
	               		}%>
	               </select>
	    		</div>
	    	</div>
	    	<div class="row">
        		<div class="col">
        			<div class="row">
        				<div class="col">
        					<button id="agregarEmpleado" type="button" data-toggle="modal" data-target="#exampleModal"class="btn btn-success">Añadir empleado</button>
        					<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Añadir empleado</h5>
							      </div>
							      <div class="modal-body">
							        ¿Estás seguro de que quieres añadir este empleado?
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
							        <button type="submit" class="btn btn-success">Añadir empleado</button>
							      </div>
							    </div>
							  </div>
							</div>
        				</div>
        			</div>	
        			<div class="row">
        				<div class="col">
        					<span>Los campos con * son obligatorios</span>
        				</div>
        			</div>
	    		</div>
	    	</div>
    	</form>
    </div>
</body>
</html>
