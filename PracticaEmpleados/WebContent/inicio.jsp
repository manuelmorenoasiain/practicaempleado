<%@ page language="java" contentType="text/html; charset=UTF-8"
     pageEncoding="UTF-8"%>
    <%@ page import ="java.util.ArrayList"%>
    <%@ page import ="clases.Empleado"%>
    <%@ page import ="clases.Departamento"%>
    <%@ page import ="java.lang.NullPointerException"%>
	<%
	String error_empleado = (String) request.getAttribute("error_empleado");
	String eliminarCorrecto = (String) request.getAttribute("eliminarCorrecto");
	if(eliminarCorrecto == null) {
		eliminarCorrecto = "";
	}
	String eliminarFallido = (String) request.getAttribute("eliminarFallido");
	if(eliminarFallido == null) {
		eliminarFallido = "";
	}
	ArrayList<Departamento> alDept = new ArrayList<Departamento>();  
	alDept = (ArrayList<Departamento>) request.getAttribute("alDept");
	%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>RRHH</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link rel="stylesheet" href="/PracticaEmpleados/rrhh.css" type="text/css">
<style>
	@media only screen and (max-width: 1024px) {
	  .col-sm-3 {
	    width: 100%;
	  }
	}
</style>
</head>
<body>
	<div id="header">
		<h1>EMPLEADOS</h1>
	</div>
	<div class="container-fluid">
		<%if(error_empleado != null) {
			%><%=error_empleado %>
		<%} else {
			%>
			<%=eliminarCorrecto %>
			<%=eliminarFallido %>
			<%ArrayList<Empleado> mostrarEmpleados = (ArrayList<Empleado>) request.getAttribute("mostrarEmpleados"); %>
		<div class="row">
			<div class="col-md-6 col-lg-1">
				<nav id="sidebar">
					<h3 id="sidebarHeader">Filtros</h3>
					<hr>
					<form action="AgregarEmpleado">
						<button type="submit" class="btn btn-success">Añadir empleado</button>
					</form>
					<hr>
			        <form method="POST" action="Inicio">
			        	<div class="row">
			        		<div class="col">
			        			<input class="form-control" type="text" name="buscarEmpNom" placeholder="Nombre de empleado"/>
			        		</div>
			        	</div>
			            <div class="row">
			            	<div class="col">
			            		<input class="form-control" type="text" name="buscarEmpSalMin" placeholder="Salario mínimo"/>
			            	</div>
			            	<div class="col">
			            		<input class="form-control" type="text" name="buscarEmpSalMax" placeholder="Salario máximo"/>
			            	</div>
			        	</div>
			        	<div class="row">
			            	<div class="col">
			            		<input class="form-control" type="text" name="buscarEmpFechaMin" placeholder="Fecha mínima"/>
			            	</div>
			            	<div class="col">
			            		<input class="form-control" type="text" name="buscarEmpFechaMax" placeholder="Fecha máxima"/>
			            	</div>
			        	</div>
			        	<div class="row">
			        		<div class="col">
			        			<select class="form-control" name="depto">
				               		<option value="">Selecciona un departamento...</option>
				               		<%for(int i=0; i<alDept.size(); i++) {
				               			%><option value="<%=alDept.get(i).getName()%>"><%=alDept.get(i).getName()%></option><%
				               		}%>
	               				</select>
			        		</div>
			        	</div>
			            <button class="btn btn-success" type="submit" name="buscar">Buscar empleados</button>
			            <hr>
			            <button class="btn btn-success" type="submit" name="mostrarTodos">Mostrar todos</button>
			        </form>
			    </nav>
			</div>
			<div class="col-md-6 col-lg-8 offset-lg-3">
				<table class="table">
					<thead class="thead-dark">
					    <tr>
					      <th scope="col">ID</th>
					      <th scope="col">Nombre</th>
					      <th scope="col">Oficio</th>
					      <th scope="col">Manager</th>
					      <th scope="col">Fecha de contratación</th>
					      <th scope="col">Salario</th>
					      <th scope="col">Comisión</th>
					      <th scope="col">Departamento</th>
					      <th scope="col"></th>
					    </tr>
					</thead>
					<tbody>
						<%
						if(mostrarEmpleados.isEmpty()) {
							%><div class='alert alert-danger' role='alert'>No hay empleados que coincidan con esos parámetros.</div><%
						} else {
							for(int i=0; i<mostrarEmpleados.size(); i++) {
								%><tr>
									<th scope="row"><%=mostrarEmpleados.get(i).getId() %></th>
									<td><%=mostrarEmpleados.get(i).getName() %></td>
									<td><%=mostrarEmpleados.get(i).getJob() %></td>
									<td><%=mostrarEmpleados.get(i).getManager() %></td>
									<td><%=mostrarEmpleados.get(i).getHiredate() %></td>
									<td><%=mostrarEmpleados.get(i).getSalary() %></td>
									<td><%=mostrarEmpleados.get(i).getCommission() %></td>
									<td><%=mostrarEmpleados.get(i).getDepartment() %></td>
									<td>
										<div class="btn-group-vertical">
										  <form action="ModificarEmpleado">
										  	<button 
										  		type="submit" 
										  		class="btn btn-info" 
										  		value="<%=mostrarEmpleados.get(i).getId()%>"
										  		name="modEmpleado">
										  		Modificar
										  	</button>
										  </form>
										  <form action="Inicio" method="POST" onsubmit="return confirm('¿Estás seguro de que quieres eliminar ese empleado?');">
									        <button
									            type="submit"
									            value="<%=mostrarEmpleados.get(i).getId()%>"
									            name="eliminarEmpleado"
									            class="btn btn-danger">
									            Eliminar
									        </button>
										  </form>
										</div>
									</td>
								  <tr>
								<%
								}
							}%>
			  		</tbody>
				</table>
			</div>
		</div>
		<%
		}
		%>
	</div>
</body>
</html>
